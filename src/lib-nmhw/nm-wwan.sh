#!/usr/bin/env bash


wwan_services_stop () {
    echo "Inhibiting wwan service"
    kill -s USR1 $(</run/wwan0.pid)
}

wwan_services_restart () {
    echo "Restarting wwan service"
    kill -s USR2 $(</run/wwan0.pid)
    systemctl restart wwan-config@$DEVICE
}
