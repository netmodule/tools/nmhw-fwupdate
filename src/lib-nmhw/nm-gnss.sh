#!/usr/bin/env bash

gnss_services_stop () {
    echo "Stopping gnss-mgr"
    systemctl stop gnss-mgr
    SER2NET_CMD=$(ps | awk '{if ($5 ~ /ser2net/) {$1=$2=$3=$4=""; print $0;}}')
    if [ "x$SER2NET_CMD" != "x" ]; then
        echo "Stopping ser2net"
        killall ser2net
    fi

    GPSD_CMD=$(ps | awk '{if ($5 ~ /gpsd/) {$1=$2=$3=$4=""; print $0;}}')
    if [ "x$GPSD_CMD" != "x" ]; then
        echo "Stopping gpsd"
        killall gpsd
    fi
}

gnss_services_restart () {
    echo "Restarting gnss-mgr"
    systemctl restart gnss-mgr
}
