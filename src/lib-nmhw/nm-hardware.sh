
read -r compatible </proc/device-tree/compatible
if [[ $compatible == ti,am335x* ]]; then
    export PROFILE=netbird
elif [[ $compatible == *imx8* ]]; then
    export PROFILE=net64
elif [[ $compatible == *marvell,armada385* ]]; then
    export PROFILE=netbolt
else
    echo "Unsupported device"
    exit -1
fi


# Check if device is part of a family
device_in_family() {
    device=$1
    family=$2
    if ! [[ "$device" == "$family"* ]]; then
        return -1
    fi
}


# Returns full path to actual device to be used by updated script
get_device_path() {
    device=$1
    if [[ "$device" == "bootloader"* ]]; then
        echo -n "/dev/storage0"
    else
        echo -n "/dev/$device"
    fi
}
