#!/usr/bin/env bash


bootloader_services_stop () {
    true
}

bootloader_services_restart () {
    if systemctl list-units | grep bootloader-config; then
        systemctl restart bootloader-config
    fi
}
