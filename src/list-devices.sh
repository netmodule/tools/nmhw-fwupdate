#!/bin/bash

device_types="storage bootloader wwan gnss"

blue() {
    echo -n -e "\\E[36m$1\\E[0m"
}
green() {
    echo -n -e "\\E[32m$1\\E[0m"
}
yellow() {
    echo -n -e "\\E[33m$1\\E[0m"
}
red() {
    echo -n -e "\\E[31m$1\\E[0m"
}

deprecated() {
    sed "s/Deprecated/$(yellow \&)/gi"
}

obsolete() {
    sed "s/Obsolete/$(red \&)/gi"
}

untested() {
    sed "s/Untested/$(yellow \&)/gi"
}

filters() {
    deprecated | obsolete | untested
}

loop_through() {
    device_type=$1
    for file in /run/"$device_type"/*.config; do
        name=$(basename "$file" | sed 's/.config//')
        echo "[$(blue "$name")]"
        cat <"$file" | filters
    done
    echo
}

for device_type in $device_types; do
    service=$device_type-config
    if [ $device_type = gnss ]; then
        service=gnss-mgr
    fi
    if [ $device_type = storage ]; then
        service=storage-info
    fi

    # Ignoring disabled or absent services
    if ! systemctl list-units | grep "$service" > /dev/null; then
        continue
    fi

    if systemctl --state=activating | grep "$service" > /dev/null; then
        echo "[$(yellow hold)] $service still activating"
        while systemctl --state=activating | grep "$service" > /dev/null; do
            echo -n .
            sleep 1
        done
        echo
    fi

    if systemctl --failed | grep "$service" > /dev/null; then
        echo "[$(red fail)] $service failed to start"
    elif systemctl --state=active | grep "$service" > /dev/null; then
        echo "[ $(green ok) ] $device_type ready:"
        loop_through "$device_type"
    elif systemctl --state=dead | grep "$service" > /dev/null; then
        echo "[$(yellow warn)] $service is not running"
    else
        echo "[$(yellow warn)] $service in unknown state, contact support"
    fi
done
