#!/usr/bin/env bash

DEVICE=$1
ARCHIVE=$2

LIBDIR=/usr/lib
DL_ARCHIVE=/tmp/fw.tar.gz
DEPLOY_DIR=/tmp/fw-installer/
HISTORY=/var/log/fw-update-history

. $LIBDIR/nm-hardware.sh
. $LIBDIR/nm-gnss.sh
. $LIBDIR/nm-wwan.sh
. $LIBDIR/nm-bootloader.sh

write_history() {
    echo "[$(date)] Update of $DEVICE to $VERSION $1 $2" >> $HISTORY
}

call_flasher () {
    echo Flashing
    log_file="/var/log/fw-update-$DEVICE-$(date -Iminutes)"
    unbuffer "$DEPLOY_DIR/flasher-$PROFILE" "$(get_device_path "$DEVICE")" 2>&1 | tee "$log_file"
    if [ ${PIPESTATUS[0]} -eq 0 ]; then
        echo "Update successful"
        write_history success
        return 0
    else
        echo "Update failed"
        write_history failed "logs are located in $log_file"
        return -1
    fi
}

download_archive () {
    if [[ $ARCHIVE == http* ]]; then
        echo Downloading firmware
        wget -O "$DL_ARCHIVE" "$ARCHIVE"
        ARCHIVE="$DL_ARCHIVE"
    fi
}

usage () {
    echo "Usage: "
    echo "	$0 <device> <path to archive>"
    echo "	$0 <device> <http link to archive>"
    echo
    echo "To get a list of supported devices, execute"
    echo "	list-devices"
}

check_archive () {

    if [ ! $ARCHIVE ]; then
        echo "No archive given"
        usage && exit -1
    fi
    if [ ! -f $ARCHIVE ] || ! files=$(tar -tf $ARCHIVE) ; then
        echo "Archive is not a tar file"
        usage && exit -1
    fi

    # Check for info and flasher-$PROFILE files
    if ! echo "$files" | grep "^./info$" > /dev/null; then
        echo "Archive is missing info file"
        usage && exit -1
    fi
    if ! echo "$files" | grep "^./flasher-$PROFILE$" > /dev/null; then
        echo "Archive is missing flasher"
        usage && exit -1
    fi
}

check_device() {
    if [ -z "$DEVICE" ] || [ "$DEVICE" == "-h" ]; then
        usage
        exit 0
    fi

    if [[ $DEVICE = /dev/* ]]; then
        DEVICE=$(echo "$DEVICE" | sed 's/\/dev\///')
    fi

    if [ ! -e /dev/$DEVICE ]; then
        echo "Device $DEVICE not found"
        usage && exit -1
    fi
}

check_device
download_archive
check_archive
mkdir -p "$DEPLOY_DIR"
tar xzf "$ARCHIVE" -C "$DEPLOY_DIR"

VERSION=$(grep "version:" <"$DEPLOY_DIR/info" | grep -o '[^ ]\+$')
FAMILY=$(grep "family:" <"$DEPLOY_DIR/info" | grep -o '[^ ]\+$')

if ! device_in_family "$DEVICE" "$FAMILY"; then
    echo "$DEVICE is not of family $FAMILY"
    exit -1
fi

"${FAMILY}_services_stop"
call_flasher
ret=$?
"${FAMILY}_services_restart"

echo "Removing firmware files"
rm -rf "$DEPLOY_DIR"
rm -rf "$DL_ARCHIVE"

exit ${ret}
